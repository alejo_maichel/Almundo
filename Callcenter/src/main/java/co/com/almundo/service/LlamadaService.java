package co.com.almundo.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import co.com.almundo.model.Llamada;

import java.util.Random;

import javax.annotation.Resource;

/**
 * Clase llamada service. Expone un WS de tipo REST para la emulacion de la
 * recepcion de una llamada
 * 
 * @author larodriguezm
 *
 */
@ApplicationScoped
@Path("/llamada")
public class LlamadaService {

	public static final String QUEUE = "/jms/queue/callcenter-queue";

	@Inject
	private JMSContext context;

	@Resource(lookup = QUEUE)
	private Queue queue;

	/**
	 * Metodo generarLlamada el cual se expone por Rest por una operacion GET para
	 * simular la recepcion de una llamada al callcenter y la envia a una cola JMS
	 * para su tratamiento async
	 * 
	 * @return retorna un mensaje de exito al navegador
	 */
	@GET
	public String generarLlamada() {

		Llamada llamada = new Llamada();
		llamada.setNumeroLlamado(generarAleatorio(2111111, 8999999));
		llamada.setNumeroLlamante(generarAleatorio(2111111, 8999999));
		llamada.setDuraccion(generarAleatorio(5000, 10000));
		context.createProducer().send(queue, llamada);
		return "Llamada enviada!!!";
	}

	/**
	 * Metodo generarAleatorio el cual se utiliza para generar un aleatorio entre
	 * deos numeros
	 * 
	 * @param min
	 *            numero minimo
	 * @param max
	 *            numero maximo
	 * @return retorna un numero entre los dos valores suministrados
	 */
	private Long generarAleatorio(int min, int max) {
		Random rand = new Random();
		Integer randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum.longValue();
	}
}
