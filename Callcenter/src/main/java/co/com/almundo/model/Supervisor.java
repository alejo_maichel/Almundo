package co.com.almundo.model;

import java.io.Serializable;
import java.util.List;

/**
 * Clase Supervisor de callcenter. hereda de la clase Empleado la cual tiene los atributos basicos
 * @author larodriguezm
 *
 */
public class Supervisor extends Empleado implements Serializable {

	private static final long serialVersionUID = 7708447665429527781L;

	private List<String> listaCampanas;

	public List<String> getListaCampanas() {
		return listaCampanas;
	}

	public void setListaCampanas(List<String> listaCampanas) {
		this.listaCampanas = listaCampanas;
	}

	public void generarEstadisticas() {

	}

	public void administrarCampana() {

	}

}
