package co.com.almundo.model;

import java.io.Serializable;

/**
 * Clase Empleado la cual tiene los atributos basicos
 * @author larodriguezm
 *
 */
public class Empleado implements Serializable {

	private static final long serialVersionUID = -2035164639476355335L;

	private String nombre;
	private String apellido;
	private Long salario;
	private Boolean disponible;

	public Long getSalario() {
		return salario;
	}

	public void setSalario(Long salario) {
		this.salario = salario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Boolean getDisponible() {
		return disponible;
	}

	public void setDisponible(Boolean disponible) {
		this.disponible = disponible;
	}

}
