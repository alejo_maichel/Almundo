package co.com.almundo.model;

import java.io.Serializable;

/**
 * Clase llamada la cual simula la llamada entrante al callcenter
 * @author larodriguezm
 *
 */
public class Llamada implements Serializable {

	private static final long serialVersionUID = 760207285369021150L;

	private Long numeroLlamante;

	private Long numeroLlamado;

	private Long duraccion;

	public Long getNumeroLlamante() {
		return numeroLlamante;
	}

	public void setNumeroLlamante(Long numeroLlamante) {
		this.numeroLlamante = numeroLlamante;
	}

	public Long getNumeroLlamado() {
		return numeroLlamado;
	}

	public void setNumeroLlamado(Long numeroLlamado) {
		this.numeroLlamado = numeroLlamado;
	}

	public Long getDuraccion() {
		return duraccion;
	}

	public void setDuraccion(Long duraccion) {
		this.duraccion = duraccion;
	}

}
