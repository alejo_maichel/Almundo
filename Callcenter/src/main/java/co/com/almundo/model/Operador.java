package co.com.almundo.model;

import java.io.Serializable;

/**
 * Clase Operador de callcenter. hereda de la clase Empleado la cual tiene los atributos basicos
 * @author larodriguezm
 *
 */
public class Operador extends Empleado implements Serializable{

	private static final long serialVersionUID = -6564011740785632878L;

	private String extension;

	private String campana;

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getCampana() {
		return campana;
	}

	public void setCampana(String campana) {
		this.campana = campana;
	}

}
