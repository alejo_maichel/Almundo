package co.com.almundo.business;

import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import co.com.almundo.model.Director;
import co.com.almundo.model.Operador;
import co.com.almundo.model.Supervisor;

/**
 * Clase Callcenter: es una clase tipo singleton la cual se inicializa con el
 * primer mensaje para controlar los operadores supervisor y director del
 * callcenter
 * 
 * @author larodriguezm
 *
 */
public class Callcenter {

	private static final Integer CANTIDADAGENTES = 5;

	private static final Logger logger = Logger.getLogger(Callcenter.class);

	private List<Operador> listaOperadores;
	private Supervisor supervisor;
	private Director director;

	private static Callcenter instance = null;

	/**
	 * Metod para obtener una unica instancia de la clase Callcenter
	 * 
	 * @return
	 */
	public static Callcenter getInstance() {
		if (instance == null) {
			instance = iniciarCallcenter();
		}
		return instance;
	}

	public List<Operador> getListaOperadores() {
		return listaOperadores;
	}

	public void setListaOperadores(List<Operador> listaOperadores) {
		this.listaOperadores = listaOperadores;
	}

	public Supervisor getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Supervisor supervisor) {
		this.supervisor = supervisor;
	}

	public Director getDirector() {
		return director;
	}

	public void setDirector(Director director) {
		this.director = director;
	}

	/**
	 * Metodo para inicializar los atributos de callcenter
	 * 
	 * @return una unica clase callcenter para el singleton
	 */
	private static Callcenter iniciarCallcenter() {

		logger.info("====================== INICIANDO CALLCENTER");
		Callcenter callcenter = new Callcenter();
		callcenter.setListaOperadores(iniciarOperadores("Campana Almundo"));
		callcenter.setSupervisor(iniciarSupervisor("Campana Almundo"));
		callcenter.setDirector(iniciarDirector());
		return callcenter;
	}

	/**
	 * Metodo que inicializa los operadores segun la cantidad definida en la
	 * variable statica de la clase CANTIDADAGENTES
	 * 
	 * @param campana
	 * @return
	 */
	private static List<Operador> iniciarOperadores(String campana) {

		List<Operador> operadores = new ArrayList<>();

		for (int a = 0; a < CANTIDADAGENTES; a++) {
			Operador operador = new Operador();
			operador.setNombre("Operador");
			operador.setApellido("Puesto " + a);
			operador.setCampana(campana);
			operador.setDisponible(true);
			operador.setExtension("100" + a);
			operador.setSalario(800000L);
			operadores.add(operador);
			logger.info("++++++++++INICIANDO OPERADOR");
		}

		return operadores;
	}

	/**
	 * Metodo para inicializar un supervisor de Callcenter
	 * @param campana
	 * @return
	 */
	private static Supervisor iniciarSupervisor(String campana) {

		Supervisor supervisor = new Supervisor();
		supervisor.setNombre("Supervisor");
		supervisor.setApellido(campana);
		supervisor.setDisponible(true);
		supervisor.setSalario(1800000L);

		List<String> camapans = new ArrayList<>();
		camapans.add(campana);
		supervisor.setListaCampanas(camapans);
		logger.info("++++++++++INICIANDO SUPERVISOR");

		return supervisor;
	}

	/**
	 * Metodo para inicializar un director de Callcenter
	 * @return 
	 */
	private static Director iniciarDirector() {

		Director director = new Director();
		director.setNombre("Directivo");
		director.setApellido(" ====== ");
		director.setDisponible(true);
		director.setSalario(2800000L);
		logger.info("++++++++++INICIANDO DIRECTOR");

		return director;
	}

}
