package co.com.almundo.business;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;

import org.jboss.logging.Logger;

import co.com.almundo.model.Empleado;
import co.com.almundo.model.Llamada;

/**
 * Clase encargada de asignar las llamadas a los agentes disponibles del
 * callcenter
 * 
 * @author larodriguezm
 *
 */
@Stateless
public class Dispatcher {

	private static final Logger logger = Logger.getLogger(Dispatcher.class);

	private Callcenter calcenter;

	@Inject
	private JMSContext context;

	@Resource(lookup = "java:/jms/queue/callcenter-queue")
	private Queue queue;

	@PostConstruct
	private void iniciarReceporMDB() {
		calcenter = Callcenter.getInstance();
	}

public void testDispatcher() {
		calcenter = Callcenter.getInstance();
	}
	/**
	 * Metodo encargado de recibir una llamada y asignar a un agente que este
	 * disponible de lo contratio la retorna a la cola de mensajes
	 * 
	 * @param llamada
	 * @return
	 */
	public boolean dispatchCall(Llamada llamada) {
		boolean llamadaAsignada = false;

		//Existe un operador disponible para recibir la llamada
		for (Empleado empleado : calcenter.getListaOperadores()) {
			if (empleado.getDisponible()) {
				try {
					logger.fatal("===== ASIGNANDO LLAMADA A Operador " + empleado.getApellido());
					empleado.setDisponible(false);
					Thread.sleep(llamada.getDuraccion());
					empleado.setDisponible(true);
					llamadaAsignada = true;
					break;
				} catch (InterruptedException e) {
					logger.fatal("Error en el hilo de espera de la llamada ", e);
				}
			}
		}

		if (!llamadaAsignada) {
			if (calcenter.getSupervisor().getDisponible()) {
				// El supervisor esta disponible para recibir una llamada
				logger.fatal("===== ASIGNANDO LLAMADA AL SUPERVISOR =====");
				try {
					calcenter.getSupervisor().setDisponible(false);
					Thread.sleep(llamada.getDuraccion());
					calcenter.getSupervisor().setDisponible(true);
					llamadaAsignada = true;
				} catch (InterruptedException e) {
					logger.fatal("Error en el hilo de espera de la llamada ", e);
				}
			} else if (calcenter.getDirector().getDisponible()) {
				// El Director esta disponible para recibir una llamada
				logger.fatal("===== ASIGNANDO LLAMADA AL DIRECTOR =====");
				try {
					calcenter.getDirector().setDisponible(false);
					Thread.sleep(llamada.getDuraccion());
					calcenter.getDirector().setDisponible(true);
					llamadaAsignada = true;
				} catch (InterruptedException e) {
					logger.fatal("Error en el hilo de espera de la llamada ", e);
				}
			}
		}
		// Si la llamada no fue recibida por ninguno se retorna a la cola de mensajes
		if (!llamadaAsignada) {
			context.createProducer().send(queue, llamada);
			logger.fatal("===== ENCOLAR LLAMADA =====");
			return false;
		}
		logger.fatal("===== FINALIZAR LLAMADA ====");

		return llamadaAsignada;

	}

}
