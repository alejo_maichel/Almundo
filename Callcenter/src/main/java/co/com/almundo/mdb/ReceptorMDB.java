package co.com.almundo.mdb;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.jboss.logging.Logger;

import co.com.almundo.business.Callcenter;
import co.com.almundo.business.Dispatcher;
import co.com.almundo.model.Llamada;

/**
 * Clase ReceptorMDB clase escucha de la cola JMS para su tratamiento y
 * asignacion de un empleado
 * 
 * @author larodriguezm
 *
 */
@MessageDriven(name = "ReceptorMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = ReceptorMDB.QUEUE),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"), })
public class ReceptorMDB implements MessageListener {

	public static final String QUEUE = "/jms/queue/callcenter-queue";
	private static final Logger logger = Logger.getLogger(Callcenter.class);

	@EJB
	private Dispatcher dispatcher;

	/**
	 * Metodo implementado de la clase MessageListener para recepcionar los mensajes de las cola 
	 * Validar que si sean del tipo Lllamda y enviarlos a la clase Dispatcher
	 */
	@Override
	public void onMessage(Message message) {
		try {
			Llamada llamada = (Llamada) ((ObjectMessage) message).getObject();
			logger.info("================================================");
			logger.info("Numero quien llama: " + llamada.getNumeroLlamado());
			logger.info("Numero a donde llama: " + llamada.getNumeroLlamante());
			logger.info("Duracion: " + llamada.getDuraccion());
			logger.info("================================================");
			dispatcher.dispatchCall(llamada);
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}

}
