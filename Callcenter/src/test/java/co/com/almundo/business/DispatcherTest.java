package co.com.almundo.business;

import java.util.Random;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.wildfly.swarm.Swarm;
import org.wildfly.swarm.arquillian.CreateSwarm;
import org.wildfly.swarm.messaging.MessagingFraction;

import co.com.almundo.model.Director;
import co.com.almundo.model.Empleado;
import co.com.almundo.model.Llamada;
import co.com.almundo.model.Operador;
import co.com.almundo.model.Supervisor;

@RunWith(Arquillian.class)
public class DispatcherTest {

	@EJB
	private Dispatcher dispatcher;

	@Inject
	private JMSContext context;
	
	@Resource(mappedName = "java:/jms/queue/callcenter-queue", name = "/jms/queue/callcenter-queue")
	private Queue queue;

	@CreateSwarm
	public static Swarm newContainer() throws Exception {
		return new Swarm().fraction(new MessagingFraction().server("default", server -> {
			server.inVmConnector("in-vm", invm -> {
				invm.serverId(1);
			});
			server.inVmAcceptor("in-vm",invm -> {
				invm.serverId(1);
			});
			server.connectionFactory("InVmConnectionFactory", factory -> {
				factory.entries("jboss/ConnectionFactory");
				factory.connectors("in-vm");
			});
			server.pooledConnectionFactory("activemq-ra", pool -> {
				pool.transaction("xa");
				pool.entries("jboss/DefaultJMSConnectionFactory");
				pool.connectors("in-vm");
			});
			server.jmsQueue("callcenter-queue");

		}));
	}

	@Deployment
	public static WebArchive createDeployment() {
		return ShrinkWrap.create(WebArchive.class).addClass(Dispatcher.class).addClass(Queue.class)
				.addClass(Callcenter.class).addClass(Llamada.class).addClass(Operador.class).addClass(Supervisor.class)
				.addClass(Director.class).addClass(Empleado.class)
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}
	
	@Test
	public void generarLlamadaQueue() throws Exception {
		Llamada llamada = new Llamada();
		llamada.setNumeroLlamado(generarAleatorio(2111111, 8999999));
		llamada.setNumeroLlamante(generarAleatorio(2111111, 8999999));
		llamada.setDuraccion(generarAleatorio(5000, 10000));

		if(context != null) {
			context.createProducer().send(queue, llamada);
			Assert.assertTrue(true);
		}else {
			Assert.assertTrue(false);
		}		
	}
	
	@Test
	public void generarLlamada() throws Exception {
		Llamada llamada = new Llamada();
		llamada.setNumeroLlamado(generarAleatorio(2111111, 8999999));
		llamada.setNumeroLlamante(generarAleatorio(2111111, 8999999));
		llamada.setDuraccion(generarAleatorio(5000, 10000));
		dispatcher.testDispatcher();
		
		for (int i = 0; i < 10; i++) {
			new Thread(() -> {
				dispatcher.dispatchCall(llamada);
			}).start();
		}
	}

	private Long generarAleatorio(int min, int max) {
		Random rand = new Random();
		Integer randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum.longValue();
	}

}

