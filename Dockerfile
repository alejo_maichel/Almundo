FROM java:openjdk-8-jdk
WORKDIR /app
ADD Callcenter/target/Callcenter-swarm.jar /app
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app/Callcenter-swarm.jar"]
